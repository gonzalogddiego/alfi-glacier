from firedrake import *
from alig import *
import numpy as np
import os

class ExpFStationary(NavierStokesProblem):
    def __init__(self, baseN, H, L, sigma, a,
                g_val, rho_val, A_val, ng_val, alpha):
        super().__init__()
        self.baseN = baseN
        self.H = H
        self.L = L
        self.sigma = sigma
        self.a = a
        self.g_val = g_val
        self.rho_val = rho_val
        self.A_val = A_val
        self.ng_val = ng_val
        self.alpha = alpha

    # mesh stretching Function
    def stretch(self, z):
        return 1-z/float(self.H)

    def bed(self, x):
        return self.a*np.exp(-((x-0.5*self.L)/self.sigma)**2)

    def mesh(self, distribution_parameters):
        base = RectangleMesh(self.baseN, self.baseN, self.L, self.H,
                            distribution_parameters=distribution_parameters)
        return base

    def DeformMesh(self, mesh):
        coords = mesh.coordinates.dat.data
        coords[:,1] = coords[:,1] + self.stretch(coords[:,1])*self.bed(coords[:,0])

    def VerticleVelProfile(self, domain):
        (x, y) = SpatialCoordinate(domain)
        fy = self.rho_val*self.g_val*sin(self.alpha)
        dh_n = self.H**(self.ng_val + 1) - (self.H-y)**(self.ng_val + 1)
        u = 2*self.A_val*(fy**self.ng_val)/(self.ng_val+1)*dh_n
        return u

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), Constant((0.0,0.0)), 3),
               DirichletBC(Z.sub(0).sub(0), self.VerticleVelProfile(Z.ufl_domain()), (1,2)),
               DirichletBC(Z.sub(0).sub(1), Constant(0.0), (1,2))]
        return bcs

    def rhs(self, Z):
        f = self.rho_val*as_vector([self.g_val*sin(self.alpha), -self.g_val*cos(self.alpha)])
        return [f,Constant(0.0)]

    def has_nullspace(self): return False

    def relaxation_direction(self): return "0+:1-"

    def mesh_hierarchy(self, hierarchy, nref, callbacks, distribution_parameters):
        baseMesh = self.mesh(distribution_parameters)
        if hierarchy == "bary":
            mh = BaryMeshHierarchy(baseMesh, nref, callbacks=callbacks,
                                   reorder=True, distribution_parameters=distribution_parameters)
        elif hierarchy == "uniformbary":
            bmesh = Mesh(bary(baseMesh._plex), distribution_parameters={"partition": False})
            mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        elif hierarchy == "uniform":
            mh = MeshHierarchy(baseMesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        else:
            raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")

        for (i, m) in enumerate(mh):
            self.DeformMesh(m)
        return mh

if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--A", type=float, default="2e-7")
    parser.add_argument("--H", type=float, default="1000")
    args, _ = parser.parse_known_args()

    # domain
    H = args.H
    beta = 100
    L = beta*H
    sigma = 0.1*beta*H
    a = 0.1*H

    ### Physical parameters
    # Glen's law exponent
    ng_val = Constant(1.0)

    # regularsation to stop stress blowing up as strain goes to zero
    eps_min_val =  Constant(1.0e-2)

    # RHS
    alpha = 3./180*np.pi
    rho_val =  Constant(910) # 917
    g_val = Constant(9.81)

    # Viscosity parameter
    A_val = Constant(args.A)

    # Initialise problem and run solver
    problem = ExpFStationary(args.baseN, H, L, sigma, a,
                            g_val, rho_val, A_val, ng_val, alpha)
    solver = get_solver(args, problem)

    # Set monitor
    error = []
    def mymonitor(ksp, it, rnorm):
        error.append(rnorm)
    solver.solver.snes.ksp.setMonitor(mymonitor)

    # Solve
    (z, results) = solver.solve(A_val, ng_val, eps_min_val)

    # Plot error
    from matplotlib import pylab
    pylab.plot(error)
    pylab.yscale('log')
    pylab.grid(True)
    pylab.savefig("output/error_%d.png"%beta)

    # Plot fcns
    (u,p) = z.split()
    File("output/u.pvd").write(u)
    mu_final = physics.eta(u, eps_min_val, A_val, ng_val)

    V = FunctionSpace(solver.mesh, "CG", 1)
    ux = Function(V); ux.interpolate(u[0])
    uy = Function(V); uy.interpolate(u[1])
    mu = Function(V); mu.interpolate(mu_final)
    File("output/ux.pvd").write(ux)
    File("output/uy.pvd").write(uy)
    File("output/p.pvd").write(p)
    File("output/mu.pvd").write(mu)

    print(u([0.1,999.9])[0])
    print(A_val.dat.data[0]*rho_val.dat.data[0]*g_val.dat.data[0]*H**2*np.sin(alpha))
