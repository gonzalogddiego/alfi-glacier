# ======================================================================
# physics.py
#
# Contains viscosity formulation etc.
#
# ======================================================================

from firedrake import *

# ======================================================================

def epsilon(u):
    """
    Strain-rate
    """

    strain   = sym(grad(u))
    return strain

# ======================================================================

def eta(u, eps_min_val, A_val, ng_val):
    """Strain-rate dependent viscosity:
       0.5*A(T)^{-1/ng}*eps_2*((1-ng)/(2*ng))"""
 
    eps_2 = 0.5 * tr(dot(epsilon(u), epsilon(u))) + eps_min_val**2.0
    #eps_2 = eps_min_val**2.0	
    visc = 0.5 * A_val**(-1.0/ng_val) * eps_2**((1.0-ng_val)/(2.0*ng_val)) 
    return visc


def eta_prime(u, eps_min_val, A_val, ng_val):
    """Strain-rate dependent viscosity:
       0.5*A(T)^{-1/ng}*eps_2*((1-ng)/(2*ng))"""
 
    eps_2 = 0.5 * tr(dot(epsilon(u), epsilon(u))) + eps_min_val**2.0
    #eps_2 = eps_min_val**2.0	
    visc = (1.0 - ng_val)/(2.0*ng_val) * 0.5 * A_val**(-1.0/ng_val) * eps_2**((1.0-3.0*ng_val)/(2.0*ng_val)) 
    return visc

# ======================================================================

def stress(p, u_prev, u_soln, eps_min_val, A_val, ng_val, V):
    """Total stress tensor"""

    try:
        dim = u_prev.ufl_domain().geometric_dimension()
    except:
        dim = u_prev.domain().geometric_dimension()

    stress =  2.0*eta(u_prev, eps_min_val, A_val, ng_val)*epsilon(u_soln)- p*Identity(dim)
    return stress


# ======================================================================

def traction(p, u_prev, u_soln, n, eps_min_val, A_val, ng_val, V):
    """
    Traction vector
    sigma.n
    """

    sigma = stress(p, u_prev, u_soln, eps_min_val, A_val, ng_val, V)
    return dot(sigma, n)


# ======================================================================

def A0(T):
    '''
    Calculate A as a function of temperature (degrees celsius). 
    '''
    TKelv = T + 273.16

    # a0= 5.3e-15 % s^-1 kPa^-3
    # a0= 5.3e-24 % s-1 Pa-3 = m+3 s+5 kg-3
    a0 = 5.3e-15*365.25*24*60*60  # yr-1 kPa-3 this is value at T=0
    
    fa=1.2478766e-39 * exp(0.32769*TKelv) + 1.9463011e-10 * exp(0.07205*TKelv)
    # Smith & Morland 1982 ,  Keine Einheiten; fa(273.15)=1.0 i.e. AGlen = A0
    
    A0=a0*fa
   
    return A0

# ========================================================================


def calc_tau_b(param, u, C_val):
    """
    Calcualte tau_b
    doesn't matter whether u_tang or not as at base where interested only cmpt is u_tang. And not used in sliding law. 
    """  
   
    m_val        =  param['m_val']
    u_min_val    =  param['u_min_val']
    V = param['velocity_function_space']
    bed = param['bed']
    #gradb = grad(bed)
    #n =  project(as_vector([gradb[0], gradb[1], -1.0]), V)
    #unit_n = project(n/sqrt(dot(n,n)),V)

    #u_tang = u - dot(u, unit_n)*unit_n
    reg = u_min_val*u_min_val

    tau_b = project(((C_val+0.0001)**(-1.0/m_val))*(inner(u,u)+ reg)**((1.0- m_val)/(2.0*m_val))*u, V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))
    #tau_b = project(((C_val)**(-1.0/m_val))*((inner(u,u)+ reg)**((1.0- m_val)/(2.0*m_val)))*u, V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))
        
    #plot(tau_b, interactive=True)     
    
    return tau_b

# ========================================================================


def calc_u(param, u, C_val):
    """
    Calcualte u from sliding law
    does it blow up like in matlab? 
    """  
   
    m_val        =  param['m_val']
    u_min_val    =  param['u_min_val']
    V = param['velocity_function_space']
    bed = param['bed']
   
    reg = u_min_val*u_min_val

    tau_b = project(((C_val+0.0001)**(-1.0/m_val))*((inner(u,u)+ reg)**((1.0- m_val)/(2.0*m_val)))*u, V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))

    #tau_b = project(((C_val)**(-1.0/m_val))*((inner(u,u)+ reg)**((1.0- m_val)/(2.0*m_val)))*u, V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))
        
    u_output = project(C_val*(inner(tau_b,tau_b)**((m_val - 1.0)/2.0))*tau_b, V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))
    
    return u_output

# ========================================================================


def calc_tau_drv(param):

    """
    Calcualte tau_d
    """
    V = param['velocity_function_space']
    rho_val   = param['rho_val']
    g_val     = param['g_val']
    surf = param['surf']
    bed = param['bed']
    H     = surf - bed
    
    gradS = grad(surf)   
    #gradS = as_vector([-sin(0.05),0.0, 0.0])
  
    tau_drv = project(rho_val*(9.81/1000.0)*H*gradS, V, solver_type="cg", preconditioner_type="amg")
    
    #plot(tau_drv, interactive=True)
    return tau_drv

# ========================================================================

def tangential_component_stress(param, w):
    """
    Calculate tangential to bed (or wherever) component of traction
    
    tau_b = -T(sigma.n) where n points away from bed.
    i.e. here use tau_b = T(sigma.n) where n points into domain. 
    
    """
    u = split(w)[0]
    p = split(w)[1]

    eps_min_val = param['eps_min_val']
    A_val = param['A_val']
    ng_val = param['ng_val']
    V = param['velocity_function_space']
    P = param['lagrange_function_space']
    bed = param['bed']
    gradb = grad(bed)
    n =  project(as_vector([-gradb[0], -gradb[1], 1.0]), V)  
    unit_n = project(n/sqrt(dot(n,n)),V)
    

    trac = traction(p, u, u, unit_n, param)

    stress_tangential = trac - dot(trac, unit_n)*unit_n
    #stress_tangential = trac

    # return the values for further analysis :
    return project(stress_tangential,V ,solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))

# ========================================================================

def skin_drag(param, w):
    u = w.split()[0] 
    p = w.split()[1]
    bed = param['bed']
    gradb = grad(bed)

    Q = param['pressure_function_space']
    V = param['velocity_function_space']
    P = param['lagrange_function_space']

    #n =  project(as_vector([gradb[0],gradb[1], -1.0]), V)
    n =  project(as_vector([-gradb[0],-gradb[1], 1.0]), V)
    factor = project(pow(sqrt(dot(n,n)), 3.0), Q)
    tau_xz = project(stress(p,u,u,param)[0,2],Q)
    tau_yz = project(stress(p,u,u, param)[1,2],Q)

    tau_s = project(as_vector([tau_xz/factor,tau_yz/factor, 0.0]), V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))
    #plot(tau_s, interactive=True)

    return tau_s

# ========================================================================

def form_drag(param, w):
    u = w.split()[0]
    p = w.split()[1]
    bed = param['bed']
    gradb = grad(bed) 
    P = param['lagrange_function_space']
    V = param['velocity_function_space']

    #n =  project(as_vector([gradb[0],gradb[1], -1.0]), V)
    n =  project(as_vector([-gradb[0],-gradb[1], 1.0]), V)
    factor = project(pow(sqrt(dot(n,n)), 3.0), P)

    tau_xx = project(stress(p,u,u,param)[0,0], P)
    tau_yy = project(stress(p,u,u,param)[1,1], P)
    tau_zz = project(stress(p,u,u,param)[2,2], P)
    tau_xz = project(stress(p,u,u,param)[0,2], P)
    tau_yz = project(stress(p,u,u,param)[1,2], P)
    tau_xy = project(stress(p,u,u,param)[0,1], P)

    dbdx = gradb[0]
    dbdy = gradb[1]

    tau_f = project(as_vector([(dbdx*(tau_zz - tau_xx) - dbdy*tau_xy)/factor, (dbdy*(tau_zz - tau_yy) - dbdx*tau_xy)/factor, (dbdx*tau_xz + dbdy*tau_yz)/factor]), V, solver_type="cg", preconditioner_type="amg", form_compiler_parameters=dict(quadrature_degree="auto"))
    #plot(tau_f, interactive=True)
    return tau_f
# ========================================================================
