from firedrake import *
from alig import *
import numpy as np
from ExpFStationary import ExpFStationary
import sys

class ExpFLinear(ExpFStationary):
    def __init__(self, eps_min_val, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.eps_min_val = eps_min_val

    def viscosity(self, Z):
        z = Function(Z)
        for bc in self.bcs(Z): bc.apply(z)
        u = z.split()[0]
        return physics.eta(u, self.eps_min_val, self.A_val, self.ng_val)

    def VerticleVelProfile(self, domain):
        (x, y) = SpatialCoordinate(domain)
        ng = self.ng_val
        fy = self.rho_val*self.g_val*sin(self.alpha)
        dh_n = self.H**(ng + 1) - (self.H-y)**(ng + 1)
        u = 2*self.A_val*(fy**ng)/(ng+1)*dh_n
        return u

class CPLinearSolver(solver.ConstantPressureSolver):

    def residual(self):

        u, p = split(self.z)
        v, q = TestFunctions(self.Z)

        # Define variable viscosity :
        self.mu = self.problem.viscosity(self.Z)

        # Define stress
        epsilon = physics.epsilon(u)
        tau = 2*self.mu*epsilon

        F = (
            inner(tau, grad(v))*dx
            + self.gamma * inner(cell_avg(div(u)), div(v))*dx(metadata={"mode": "vanilla"})
            - p * div(v) * dx
            - div(u) * q * dx
        )

        return F


if __name__ == "__main__":

    parser = get_default_parser()
    args, _ = parser.parse_known_args()

    # domain
    H = 1000
    beta = 100
    L = beta*H
    sigma = 0.1*beta*H
    a = 0.1*H

    ### Physical parameters
    # Glen's law exponent
    ng_val = Constant(3.0)

    # regularsation to stop stress blowing up as strain goes to zero
    eps_min_val =  Constant(1.0e-2)

    # RHS
    alpha = 0.5/180*np.pi
    rho_val =  Constant(910) # 917
    g_val = Constant(9.81)

    # Viscosity parameter
    A_val = Constant(2.14e-7)

    # Initialise problem
    problem = ExpFLinear(eps_min_val, args.baseN, H, L, sigma, a,
                            g_val, rho_val, A_val, ng_val, alpha)

    solver_t = CPLinearSolver
    solver_alig = solver_t(
            problem = problem,
            solver_type=args.solver_type,
            schur_approx=args.schur_approx,
            nref=args.nref,
            k=args.k,
            gamma=args.gamma,
            nref_vis=args.nref_vis,
            patch=args.patch,
            use_mkl=args.mkl,
            hierarchy=args.mh,
            patch_composition=args.patch_composition,
            restriction=args.restriction,
            smoothing=args.smoothing,
            rebalance_vertices=args.rebalance,
            high_accuracy=args.high_accuracy,
            hierarchy_callback=None,
        )

    # Define viscosity function
    mu = problem.viscosity(solver_alig.Z)

    # Save Viscosity
    V = FunctionSpace(solver_alig.mesh, "CG", 1)
    mu_fcn = Function(V)
    mu_fcn.interpolate(mu)
    File("mu.pvd").write(mu_fcn)

    # Define parameters for solver
    params = solver_alig.params
    params["snes_type"] = "ksponly"

    # Reconstruct solver from alig
    problem_NVP = solver_alig.problem_NVP
    nsp = solver_alig.nsp
    appctx = {"mu": mu, "gamma": args.gamma}
    solver = LinearVariationalSolver(problem_NVP, solver_parameters=params,
                                             nullspace=nsp, options_prefix="ns_",
                                             appctx=appctx)

    solver_alig.IterCount = solver.snes.ksp.getIterationNumber
    transfermanager = TransferManager(native_transfers=solver_alig.get_transfers())
    solver.set_transfer_manager(transfermanager)
    solver.solve()

    # Post processing
    z = solver_alig.z
    (u,p) = z.split()
    File("u.pvd").write(u)

    ux = Function(V); ux.interpolate(u[0])
    uy = Function(V); uy.interpolate(u[1])
    File("ux.pvd").write(ux)
    File("uy.pvd").write(uy)
    File("p.pvd").write(p)

    print(assemble(dot(u,u)*ds(3)))
