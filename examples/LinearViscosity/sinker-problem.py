from firedrake import *
from alig import *
from LinearSolver import SVLinearSolver, CPLinearSolver, SV3LinearSolver
from functools import reduce
import SchurLinear
import pprint


class LinearSinkerProblem(NavierStokesProblem):
    def __init__(self, baseN, dr):
        self.baseN = baseN
        self.dr = dr

        self.omega = 0.1
        self.delta = 200
        self.mu_min = Constant(dr**-0.5)
        self.mu_max = Constant(dr**0.5)

    def mesh(self, distp):
        base = RectangleMesh(20, 20, 4, 4, distribution_parameters=distp)
        return base

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), 0, "on_boundary")]
        return bcs

    def has_nullspace(self): return False

    def relaxation_direction(self): return "0+:1-"

    def chi_n(self, mesh):
        X = SpatialCoordinate(mesh)
        def Max(a, b): return (a+b+abs(a-b))/Constant(2)
        def indi(ci):
            return 1-exp(-self.delta * Max(0, sqrt(inner(ci-X, ci-X))-self.omega/2)**2)
        indis = [indi(Constant((4*(cx+1)/3, 4*(cy+1)/3))) for cx in range(2) for cy in range(2)]
        return reduce(lambda x, y : x*y, indis, Constant(1.0))

    def mu_expr(self, mesh):
        return (self.mu_max-self.mu_min)*(1-self.chi_n(mesh)) + self.mu_min

    def mu(self,mesh,Q):
        Qm = FunctionSpace(mesh, Q.ufl_element())
        return Function(Qm).interpolate(self.mu_expr(mesh))

    def viscosity(self, Z):
        return self.mu_expr(Z.mesh())

    def rhs(self, Z):
        f = 10 * (self.chi_n(Z.mesh())-1)*as_vector([0, 1])
        return [f,Constant(0.0)]


if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--dr", type=int, default=1e4)
    args, _ = parser.parse_known_args()

    # Initialise problem
    problem = LinearSinkerProblem(args.baseN, args.dr)

    # Construct solver
    solver_t = {"pkp0": CPLinearSolver,
                "sv": SVLinearSolver,
                "sv3": SV3LinearSolver}[args.discretisation]

    solver_alig = solver_t(
            problem = problem,
            solver_type=args.solver_type,
            schur_approx=args.schur_approx,
            nref=args.nref,
            k=args.k,
            gamma=args.gamma,
            nref_vis=args.nref_vis,
            patch=args.patch,
            use_mkl=args.mkl,
            hierarchy=args.mh,
            patch_composition=args.patch_composition,
            restriction=args.restriction,
            smoothing=args.smoothing,
            rebalance_vertices=args.rebalance,
            high_accuracy=args.high_accuracy,
            hierarchy_callback=None,
        )

    # Define viscosity function
    Q = solver_alig.Z.sub(1)
    mus = [problem.mu(m, Q) for m in solver_alig.mh]
    mu = mus[-1]

    # Define parameters for solver
    params = solver_alig.params
    params["snes_type"] = "ksponly"
    params["fieldsplit_1"] = {
        "ksp_type": "preonly",
        "pc_type": "python",
        "pc_python_type": {
            "mp" : "alig.schur.DGMassInv",
            "mnu" : "SchurLinear.DGWeightedMassInv",
            "bfbt" : "alig.schur.BFBt",
            "w_bfbt" : "alig.schur.w_BFBt"}[args.schur_approx]
    }
    #pprint.pprint(params)

    # Reconstruct solver from alig
    problem_NVP = solver_alig.problem_NVP
    nsp = solver_alig.nsp
    appctx = {"mu": mu, "gamma": args.gamma}
    solver = LinearVariationalSolver(problem_NVP, solver_parameters=params,
                                             nullspace=nsp, options_prefix="ns_",
                                             appctx=appctx)

    solver_alig.IterCount = solver.snes.ksp.getIterationNumber
    transfermanager = TransferManager(native_transfers=solver_alig.get_transfers())
    solver.set_transfer_manager(transfermanager)
    solver.solve()
