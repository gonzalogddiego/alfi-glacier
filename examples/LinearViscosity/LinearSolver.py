from alig import *
from datetime import datetime

class CPLinearSolver(solver.ConstantPressureSolver):

    def residual(self):

        u, p = split(self.z)
        v, q = TestFunctions(self.Z)

        # Define variable viscosity :
        self.mu = self.problem.viscosity(self.Z)

        # Define stress
        epsilon = physics.epsilon(u)
        tau = 2*self.mu*epsilon

        F = (
            inner(tau, grad(v))*dx
            + self.gamma * inner(cell_avg(div(u)), div(v))*dx(metadata={"mode": "vanilla"})
            - p * div(v) * dx
            - div(u) * q * dx
        )

        return F


class SVLinearSolver(solver.ScottVogeliusSolver):

    def residual(self):
        u, p = split(self.z)
        v, q = TestFunctions(self.Z)

        # Define variable viscosity :
        self.mu = self.problem.viscosity(self.Z)

        ### Define stress
        epsilon = physics.epsilon(u) # strain
        tau = 2*self.mu*epsilon

        F = (
            inner(tau, grad(v))*dx
            + self.gamma * inner(div(u), div(v))*dx
            - p * div(v) * dx
            - div(u) * q * dx
        )

        return F

class SV3LinearSolver(solver.SVThreeFieldSolver):

    def residual(self):
        u, p, s0 = split(self.z)
        v, q, t0 = TestFunctions(self.Z)

        # Define variable viscosity :
        self.mu = self.problem.viscosity(self.Z)

        ### Define strain
        epsilon = physics.epsilon(u) # strain

        # Define stress
        s = as_tensor([[s0[0], s0[1]],[s0[1], -s0[0]]])
        t = as_tensor([[t0[0], t0[1]],[t0[1], -t0[0]]])

        F = (
            inner(s, grad(v))*dx
            + self.gamma * inner(div(u), div(v))*dx
            - p * div(v) * dx
            + inner(s, t) * dx
            - self.mu * inner(epsilon, t) * dx
            - div(u) * q * dx
        )

        return F
