from firedrake import *
import numpy as np

H = 1000
N = 50
alpha = 2
L = alpha*H
sigma = 0.1*alpha*H
a = 0.1*H

mesh = RectangleMesh(N,N,L,H)
coordinates = mesh.coordinates.dat.data

# stretching Function
def stretch(z):
    return 1-z/float(H)

def bed(x):
    return a*np.exp(-((x-0.5*L)/sigma)**2)


def f(coords):
    coords[:,1] = coords[:,1] + stretch(coords[:,1])*bed(coords[:,0])

f(coordinates)

V = FunctionSpace(mesh, "CG", 1)
v = Function(V)
v.interpolate(Constant(1.0))
File("v.pvd").write(v)
