from firedrake import *
from alig import *
import numpy as np

class ExpB(NavierStokesProblem):
    def __init__(self, baseN, Lx, Ly, Amp, wavelength, g_val, rho_val):
        super().__init__()
        self.baseN = baseN
        self.Lx = Lx
        self.Ly = Ly
        self.Amp = Amp
        self.wavelength = wavelength
        self.g_val = g_val
        self.rho_val = rho_val

    def mesh(self, distribution_parameters):
        base = CylinderMesh(self.baseN, self.baseN, 1.0, 0.5,
                             distribution_parameters=distribution_parameters)
        return base

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), Constant((0.0,0.0)), (1))]
        return bcs


    def deform(self, mesh):
        coordinates = mesh.coordinates.dat.data
        coordinates[:,0] = self.Lx*coordinates[:,0]
        coordinates[:,1] = 2*self.Ly*coordinates[:,1]
        coordinates[:, 1] = coordinates[:, 1] + self.Amp*np.sin(np.pi*coordinates[:, 0]/self.wavelength)*(self.Ly-coordinates[:,1])


    def dsb(self,mesh):
        return ds(1)

    def has_nullspace(self): return False

    def rhs(self, Z):
        f = self.rho_val*self.g_val
        return [f,Constant(0.0)]

    def relaxation_direction(self): return "0+:1-"

    def mesh_hierarchy(self, hierarchy, nref, callbacks, distribution_parameters):
        baseMesh = self.mesh(distribution_parameters)
        if hierarchy == "bary":
            mh = BaryMeshHierarchy(baseMesh, nref, callbacks=callbacks,
                                   reorder=True, distribution_parameters=distribution_parameters)
        elif hierarchy == "uniformbary":
            bmesh = Mesh(bary(baseMesh._plex), distribution_parameters={"partition": False})
            mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        elif hierarchy == "uniform":
            mh = MeshHierarchy(baseMesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        else:
            raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")

        meshes = tuple(periodise(m) for m in mh)
        mh = HierarchyBase(meshes, mh.coarse_to_fine_cells, mh.fine_to_coarse_cells)
        for (i, m) in enumerate(mh):
            self.deform(m)
            if i > 0:
                snap(m, self.baseN * 2**i)
        return mh


if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--A", type=float, default="1e-16")
    parser.add_argument("--n", type=float, default="1")
    parser.add_argument("--Ly", type=float, default="1000")
    parser.add_argument("--beta", type=float, default="100")
    parser.add_argument("--amp", type=float, default="0.5")
    args, _ = parser.parse_known_args()

    # Size of domain
    Ly = args.Ly
    Lx = args.beta*Ly

    # Magnitude of domain perturbation
    Amp = -args.amp
    wavelength = 0.5*Lx

    ### Physical parameters
    # Glen's law exponent
    ng_val = Constant(args.n)

    # regularsation to stop stress blowing up as strain goes to zero
    eps_min_val =  Constant(1.0e-2)

    # RHS
    alpha = 0.5/180*np.pi
    rho_val =  Constant(910) # 917
    g_val = as_vector([9.81*sin(alpha), -9.81*cos(alpha)])

    # Viscosity parameter
    A_val = Constant(args.A)

    # Initialise problem and run solver
    problem = ExpB(args.baseN, Lx, Ly, Amp, wavelength, g_val, rho_val)
    solver = get_solver(args, problem)

    # Set monitor
    error = []
    def mymonitor(ksp, it, rnorm):
        error.append(rnorm)
    solver.solver.snes.ksp.setMonitor(mymonitor)

    # Solve
    (z, results) = solver.solve(A_val, ng_val, eps_min_val)


    # Plot error
    from matplotlib import pylab
    pylab.plot(error)
    pylab.yscale('log')
    pylab.grid(True)
    pylab.savefig("output/error_%d.png"%args.beta)

    # Plotting fcns
    (u,p) = z.split()
    File("output/u.pvd").write(u)
    mu_final = physics.eta(u, eps_min_val, A_val, ng_val)

    V = FunctionSpace(solver.mesh, "CG", 1)
    ux = Function(V); ux.interpolate(u[0])
    uy = Function(V); uy.interpolate(u[1])
    mu = Function(V); mu.interpolate(mu_final)
    File("output/ux.pvd").write(ux)
    File("output/uy.pvd").write(uy)
    File("output/p.pvd").write(p)
    File("output/mu.pvd").write(mu)

    # Print surface velocity to compare with theory
    ux_surface = u([0.01*Lx,0.99*Ly])[0]
    ux_unperturbed = 2*A_val.dat.data[0]*(rho_val.dat.data[0]*9.81*np.sin(alpha))**(args.n)*Ly**(args.n+1)/(args.n+1)
    print("Horizontal velocity on surface at x = 0 :: %e" %ux_surface )
    print("Theoretical unperturped horizontal velocity on surface :: %e" %ux_unperturbed )
