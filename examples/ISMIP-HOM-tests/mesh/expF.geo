ls = 50;

H0 = 1000;
alpha = 10;

L = alpha*H0;
a0 = 0.1*H0;
sigma = alpha/10.0*H0;

Point(1) = {0, -H0, 0, ls};
Point(2) = {0, 0, 0, ls};
Point(3) = {L, 0, 0, ls};
Point(4) = {L, -H0, 0, ls};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
// Line(4) = {4,1};

pList[0] = 1;
nPoints = 20; //Ceil(L/ls);
For i In {1 : nPoints}
  x = L*i/(nPoints + 1);
  pList[i] = newp;
  Point(pList[i]) = {x,
		 ( - H0 + a0 * Exp( - (x - L/2.0)^2 / sigma^2 ) ),
		 0, ls};
  EndFor
pList[nPoints + 1] = 4;

Spline(newl) = pList[];

Line Loop(5) = {1,2,3,-4};
Plane Surface(6) = {5};

Physical Line("Inflow") = {1};
Physical Line("FreeSurface") = {2};
Physical Line("Outflow") = {3};
Physical Line("NoSlip") = {4};
Physical Surface("Sheet") = {6};
