from setuptools import setup

setup(name="AliG",
      version="0.0.1",
      description="(A)ugmented (L)agrangian based solvers for (I)ce sheet and (G)lacier equations",
      packages = ["alig"])
