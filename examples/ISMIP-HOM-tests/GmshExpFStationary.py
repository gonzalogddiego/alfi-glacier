from firedrake import *
from alig import *
import numpy as np
import os

class ExpFStationary(NavierStokesProblem):
    def __init__(self, g_val, rho_val, A_val, ng_val, alpha):
        super().__init__()
        self.g_val = g_val
        self.rho_val = rho_val
        self.A_val = A_val
        self.ng_val = ng_val
        self.alpha = alpha

    def mesh(self, distribution_parameters):
        base = Mesh(os.path.dirname(os.path.abspath(__file__)) + "/" + "mesh/expF.msh",
                distribution_parameters=distribution_parameters)
        return base

    def VerticleVelProfile(self, domain):
        Ly = 1000
        (x, y) = SpatialCoordinate(domain)
        fy = self.rho_val*self.g_val*sin(self.alpha)
        dh_n = Ly**(self.ng_val + 1) - (-y)**(self.ng_val + 1)
        u = 2*self.A_val*(fy**self.ng_val)/(self.ng_val+1)*dh_n
        return u

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), Constant((0.0,0.0)), 4),
               DirichletBC(Z.sub(0).sub(0), self.VerticleVelProfile(Z.ufl_domain()), (1,3)),
               DirichletBC(Z.sub(0).sub(1), Constant(0.0), (1,3))]
        return bcs

    def rhs(self, Z):
        f = self.rho_val*as_vector([self.g_val*sin(self.alpha), -self.g_val*cos(self.alpha)])
        return [f,Constant(0.0)]

    def has_nullspace(self): return False

    def relaxation_direction(self): return "0+:1-"

if __name__ == "__main__":

    parser = get_default_parser()
    args, _ = parser.parse_known_args()

    ### Physical parameters
    # Glen's law exponent
    ng_val = Constant(3.0)

    # regularsation to stop stress blowing up as strain goes to zero
    eps_min_val =  Constant(1.0e-2)

    # RHS
    alpha = 0.5/180*np.pi
    rho_val =  Constant(910) # 917
    g_val = Constant(9.81)

    # Viscosity parameter
    A_val = Constant(2.14e-7)

    # Initialise problem and run solver
    problem = ExpFStationary(g_val, rho_val, A_val, ng_val, alpha)
    solver = get_solver(args, problem)
    (z, results) = solver.solve(A_val, ng_val, eps_min_val)
    (u,p) = z.split()
    File("u.pvd").write(u)

    V = FunctionSpace(solver.mesh, "CG", 1)
    ux = Function(V); ux.interpolate(u[0])
    uy = Function(V); uy.interpolate(u[1])
    File("ux.pvd").write(ux)
    File("uy.pvd").write(uy)
    File("p.pvd").write(p)

    print(assemble(dot(u,u)*ds(4)))
