from firedrake import *
from alig import *
import numpy as np
import os

class ExpFStationary(NavierStokesProblem):
    def __init__(self, baseN, H, L, sigma, a,
                g_val, rho_val, alpha):
        super().__init__()
        self.baseN = baseN
        self.H = H
        self.L = L
        self.sigma = sigma
        self.a = a
        self.g_val = g_val
        self.rho_val = rho_val
        self.alpha = alpha

    # mesh stretching Function
    def stretch(self, z):
        return 1-z/float(self.H)

    def bed(self, x):
        return self.a*np.exp(-((x-0.5*self.L)/self.sigma)**2)

    def mesh(self, distribution_parameters):
        base = CylinderMesh(self.baseN, self.baseN, 1.0, 0.5,
                             distribution_parameters=distribution_parameters)
        return base

    def DeformMesh(self, mesh):
        coords = mesh.coordinates.dat.data
        coords[:,0] = self.L*coords[:,0]
        coords[:,1] = 2*self.H*coords[:,1]
        coords[:,1] = coords[:,1] + self.stretch(coords[:,1])*self.bed(coords[:,0])

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), Constant((0.0,0.0)), 1)]
        return bcs

    def rhs(self, Z):
        f = self.rho_val*as_vector([self.g_val*sin(self.alpha), -self.g_val*cos(self.alpha)])
        return [f,Constant(0.0)]

    def has_nullspace(self): return False

    def relaxation_direction(self): return "0+:1-"

    def mesh_hierarchy(self, hierarchy, nref, callbacks, distribution_parameters):
        baseMesh = self.mesh(distribution_parameters)
        if hierarchy == "bary":
            mh = BaryMeshHierarchy(baseMesh, nref, callbacks=callbacks,
                                   reorder=True, distribution_parameters=distribution_parameters)
        elif hierarchy == "uniformbary":
            bmesh = Mesh(bary(baseMesh._plex), distribution_parameters={"partition": False})
            mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        elif hierarchy == "uniform":
            mh = MeshHierarchy(baseMesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        else:
            raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")

        meshes = tuple(periodise(m) for m in mh)
        mh = HierarchyBase(meshes, mh.coarse_to_fine_cells, mh.fine_to_coarse_cells)
        for (i, m) in enumerate(mh):
            self.DeformMesh(m)
        return mh

if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--A", type=float, default="2e-7")
    parser.add_argument("--n", type=float, default="1")
    parser.add_argument("--H", type=float, default="1000")
    parser.add_argument("--beta", type=float, default="100")
    parser.add_argument("--amp", type=float, default="0.1")
    parser.add_argument("--rho", type=float, default="910")
    args, _ = parser.parse_known_args()

    # domain
    H = args.H
    beta = args.beta
    L = beta*H
    sigma = 0.1*beta*H
    a = args.amp*H

    ### Physical parameters
    # Glen's law exponent
    ng_val = Constant(args.n)

    # regularsation to stop stress blowing up as strain goes to zero
    eps_min_val =  Constant(1.0e-2)


    # RHS
    alpha = 3./180*np.pi
    rho_val =  Constant(args.rho) # 917
    g_val = Constant(9.81)

    # Viscosity parameter
    A_val = Constant(args.A)

    # Initialise problem and run solver
    problem = ExpFStationary(args.baseN, H, L, sigma, a,
                            g_val, rho_val, alpha)
    solver = get_solver(args, problem)

    # Set monitor
    error = []
    V = FunctionSpace(solver.mesh, "CG", 1)
    mu = Function(V)
    z_sol = Function(solver.Z)
    pvd = File("output/mu_iter.pvd")
    def mymonitor(ksp, it, rnorm):
        error.append(rnorm)
        if solver.IterCount() == 0:
            xbar = solver.solver.snes.getSolution()
            with z_sol.dat.vec_wo as y:
                xbar.copy(y)
            u = z_sol.split()[0]
            mu_form = physics.eta(u, eps_min_val, A_val, ng_val)
            mu.interpolate(mu_form)
            pvd.write(mu)

    solver.solver.snes.ksp.setMonitor(mymonitor)

    # Solve problem
    (z, results) = solver.solve(A_val, ng_val, eps_min_val)

    # Plot error
    from matplotlib import pylab
    pylab.plot(error)
    pylab.yscale('log')
    pylab.grid(True)
    pylab.savefig("output/error_%d.png"%beta)

    # Plotting fcns
    (u,p) = z.split()
    File("output/u.pvd").write(u)
    mu_final = physics.eta(u, eps_min_val, A_val, ng_val)

    ux = Function(V); ux.interpolate(u[0])
    uy = Function(V); uy.interpolate(u[1])
    File("output/ux.pvd").write(ux)
    File("output/uy.pvd").write(uy)
    File("output/p.pvd").write(p)

    # Print surface velocity to compare with theory
    ux_surface = u([0.01*L,0.99*H])[0]
    ux_unperturbed = 2*A_val.dat.data[0]*(rho_val.dat.data[0]*g_val.dat.data[0]*np.sin(alpha))**(args.n)*H**(args.n+1)/(args.n+1)
    print("Horizontal velocity on surface at x = 0 :: %e" %ux_surface )
    print("Theoretical unperturped horizontal velocity on surface :: %e" %ux_unperturbed )
