from firedrake import *
import sys

class DGWeightedMassInv(PCBase):

    def initialize(self, pc):
        print("update called"); sys.stdout.flush()
        _, P = pc.getOperators()
        appctx = self.get_appctx(pc)
        self.mu = appctx["mu"]
        self.gamma = appctx["gamma"]
        V = dmhooks.get_function_space(pc.getDM())
        u = TrialFunction(V)
        v = TestFunction(V)
        nu = - self.mu- self.gamma
        massinv = assemble(Tensor(1./nu*inner(u, v)*dx).inv)
        self.massinv = massinv.petscmat

    def update(self, pc):
        pass

    def apply(self, pc, x, y):
        self.massinv.mult(x, y)

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")
