#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 11:45:07 2020

@author: gonzalezdedi
"""

from firedrake import *
from alig import *
from LinearSolver import SVLinearSolver, CPLinearSolver
import numpy as np
import matplotlib.pylab as plt
import pprint
from petsc4py import PETSc

class LidDrivenCavityFlow(NavierStokesProblem):
    def __init__(self, baseN, x_refine, Lx,
                 eps_min_val, delta_eps, regular_eta, omega,
                 var_visc_transfer = False, regularised = True, periodic = False):
        super().__init__()
        self.baseN = baseN
        self.x_refine = x_refine
        self.Lx = Lx
        self.eps_min_val = eps_min_val
        self.delta_eps = delta_eps
        self.regular_eta = regular_eta
        self.omega = omega
        self.var_visc_transfer = var_visc_transfer
        self.regularised = regularised
        self.periodic = periodic

    def mesh(self, distribution_parameters):
        scale = 1
        if self.x_refine:
            scale = int(0.5*self.Lx)
        if self.periodic :
            base = CylinderMesh(scale*self.baseN, self.baseN, 1.0, 0.5,
                                 distribution_parameters=distribution_parameters)
        else:
            base = RectangleMesh(scale*self.baseN, self.baseN, self.Lx, 2,
                                 distribution_parameters=distribution_parameters)
        return base

    def driver(self, domain):
        (x, y) = SpatialCoordinate(domain)
        if self.regularised:
            driver = as_vector([x*x*(self.Lx-x)*(self.Lx-x)*(0.25*y*y), 0])
        else:
            driver = as_vector([(0.25*y*y), 0])
        return driver

    def bcs(self, Z):
        if self.periodic:
            bcs = [DirichletBC(Z.sub(0), Constant((0.0,0.0)), 1),
                   DirichletBC(Z.sub(0), self.driver(Z.ufl_domain()), 2)]
        else:
            bcs = [DirichletBC(Z.sub(0), Constant((0.0,0.0)), [1,2,3]),
                   DirichletBC(Z.sub(0), self.driver(Z.ufl_domain()), 4)]
#        from pressureBC import PressureFixBC ; bcs.append(PressureFixBC(Z.sub(1), 0, 1))
        return bcs


    def deform(self, mesh):
        coordinates = mesh.coordinates.dat.data
        coordinates[:,0] = self.Lx*coordinates[:,0]
        coordinates[:,1] = 4*coordinates[:,1]

    def has_nullspace(self): return False

    def relaxation_direction(self): return "0+:1-"

    def viscosity(self, Z):
        if self.regular_eta == True:
            omega = self.omega
            x,y = SpatialCoordinate(Z.mesh())
            omega = 2*self.omega*np.pi
            eta = (sin(omega*y/2.)+1)*(sin(omega*x/self.Lx)+1)*self.delta_eps/4.+self.eps_min_val
            eta_space = FunctionSpace(Z.mesh(), "CG", 1)
        else:
            eta_space = FunctionSpace(Z.mesh(), "CG", 1)
            eta = Function(eta_space)
            eta_dof = np.random.rand(eta_space.dim())
            a = np.log10(self.eps_min_val+self.delta_eps)
            b = np.log10(self.eps_min_val)
            eta_dof_log = (a-b)*np.exp(np.log10(eta_dof))+b
            eta_dof = 10**eta_dof_log
            eta.vector()[:] = eta_dof

        eta_fcn = Function(eta_space)
        eta_fcn.interpolate(eta)
        max_eta = max(eta_fcn.vector()[:])
        min_eta = min(eta_fcn.vector()[:])
        print("Max eta = " + str(max_eta) + " min eta = " + str(min_eta))
        return eta


    def mesh_hierarchy(self, hierarchy, nref, callbacks, distribution_parameters):
        baseMesh = self.mesh(distribution_parameters)
        if hierarchy == "bary":
            mh = BaryMeshHierarchy(baseMesh, nref, callbacks=callbacks,
                                   reorder=True, distribution_parameters=distribution_parameters)
        elif hierarchy == "uniformbary":
            bmesh = Mesh(bary(baseMesh._plex), distribution_parameters={"partition": False})
            mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        elif hierarchy == "uniform":
            mh = MeshHierarchy(baseMesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        else:
            raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")

        if self.periodic:
            meshes = tuple(periodise(m) for m in mh)
            mh = HierarchyBase(meshes, mh.coarse_to_fine_cells, mh.fine_to_coarse_cells)
            for (i, m) in enumerate(mh):
                self.deform(m)
                if i > 0:
                    snap(m, self.baseN * 2**i)
        return mh

if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--Lx", type=int, default=2)
    args, _ = parser.parse_known_args()

    # Periodic bc
    periodic = False

    # Introduce variable viscosity in prolongation operator
    var_visc_transfer = True

    # refine along x direction to maintain cell aspect ratio?
    x_refine = False

    # Set variable viscosity
    # Viscosity in [eps_min_val, eps_min + delta_eps]. By default, CG1 with randomly distributed values
    eps_min_val = 0.01
    delta_eps = 1 - eps_min_val

    # Regularised viscosity product of sinusoidal terms with frequency omega
    regular_eta = True
    omega = 5

    # Initialise problem
    problem = LidDrivenCavityFlow(args.baseN, x_refine, args.Lx,
                                 eps_min_val, delta_eps, regular_eta, omega,
                                 var_visc_transfer = var_visc_transfer, regularised = True, periodic = periodic)

    # Construct solver
    solver_t = {"pkp0": CPLinearSolver,
                "sv": SVLinearSolver}[args.discretisation]

    solver_alig = solver_t(
            problem = problem,
            solver_type=args.solver_type,
            schur_approx=args.schur_approx,
            nref=args.nref,
            k=args.k,
            gamma=args.gamma,
            nref_vis=args.nref_vis,
            patch=args.patch,
            use_mkl=args.mkl,
            hierarchy=args.mh,
            patch_composition=args.patch_composition,
            restriction=args.restriction,
            smoothing=args.smoothing,
            rebalance_vertices=args.rebalance,
            high_accuracy=args.high_accuracy,
            hierarchy_callback=None,
            sliding = args.sliding,
            nitsche = args.nitsche,
        )

    # Define parameters for solver
    params = solver_alig.params
    params["snes_type"] = "ksponly"
    params["fieldsplit_1"] = {
        "ksp_type": "preonly",
        "pc_type": "python",
        "pc_python_type": {
            "mp" : "alig.schur.DGMassInv",
            "mnu" : "SchurLinear.DGWeightedMassInv",
            "bfbt" : "alig.schur.BFBt",
            "w_bfbt" : "alig.schur.w_BFBt"}[args.schur_approx]
    }
    #pprint.pprint(params)

    # Reconstruct solver from alig
    problem_NVP = solver_alig.problem_NVP
    nsp = solver_alig.nsp
    appctx = {"mu": solver_alig.mu, "gamma": args.gamma}
    solver = LinearVariationalSolver(problem_NVP, solver_parameters=params,
                                             nullspace=nsp, options_prefix="ns_",
                                             appctx=appctx)

    solver_alig.IterCount = solver.snes.ksp.getIterationNumber
    transfermanager = TransferManager(native_transfers=solver_alig.get_transfers())
    solver.set_transfer_manager(transfermanager)
    solver.solve()
