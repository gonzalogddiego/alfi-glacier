from firedrake import *

class BFBt(PCBase):

    def initialize(self, pc):
        appctx = self.get_appctx(pc)

        # Recover function space and boundary conditions
        Z = appctx["Z_space"]
        (u,p) = split(TrialFunction(Z))
        (v,q) = split(TestFunction(Z))
        self.u, self.v = u, v
        self.bcs = appctx["bcs"]
        (self.u_is, self.p_is) = Z._ises

        # Recover parameters
        self.A_val = appctx["A_val"]
        self.eps_min_val = appctx["eps_min_val"]
        self.ng_val = appctx["ng_val"]
        self.gamma = appctx["gamma"]

        self.update(pc)

    def update(self, pc):

        import firedrake.dmhooks
        dm = pc.getDM()
        parentdm = firedrake.dmhooks.get_parent(dm)
        appctx = firedrake.dmhooks.get_appctx(parentdm)

        J = assemble(appctx.J, mat_type="aij", bcs=appctx.bcs_J).petscmat

        # Construct matrices
        Bp = J.createSubMatrix(self.p_is, self.u_is)
        Ap = J.createSubMatrix(self.u_is, self.u_is)

        BA = PETSc.Mat.matMult(Bp,Ap)
        self.M_mat = PETSc.Mat.matTransposeMult(BA,Bp)
        BBt = PETSc.Mat.matTransposeMult(Bp,Bp)

        # Construct middle block
        self.workspace = [self.M_mat.createVecLeft() for i in (0, 1)]

        # Construct lateral block
        Nksp = PETSc.KSP().create(comm=pc.comm)
        Nksp.setOperators(BBt)
        Nksp.setUp()
        Nksp.setFromOptions()
        self.Nksp = Nksp


    def apply(self, pc, x, y):
        a, b = self.workspace
        self.Nksp.solve(x, a)
        self.M_mat.mult(a, b)
        self.Nksp.solve(b, y)

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")



class w_BFBt(PCBase):

    def initialize(self, pc):
        appctx = self.get_appctx(pc)

        # Recover function space and boundary conditions
        Z = appctx["Z_space"]
        (u,p) = split(TrialFunction(Z))
        (v,q) = split(TestFunction(Z))
        self.u, self.v = u, v
        self.bcs = appctx["bcs"]
        (self.u_is, self.p_is) = Z._ises

        # Recover parameters
        self.A_val = appctx["A_val"]
        self.eps_min_val = appctx["eps_min_val"]
        self.ng_val = appctx["ng_val"]
        self.gamma = appctx["gamma"]

        # Construct matrix B
        b = -div(u)*q*dx
        self.B = assemble(b, mat_type="aij", bcs = self.bcs)
        self.Bp = self.B.petscmat
        self.Bp = self.Bp.createSubMatrix(self.p_is, self.u_is)

        self.update(pc)

    def update(self, pc):

        # Construct viscous term
        appctx = self.get_appctx(pc)
        state = appctx["state"]
        u0 = split(state)[0]
        eps_2 = 0.5 * tr(dot(sym(grad(u0)), sym(grad(u0)))) + self.eps_min_val**2.0
        visc = self.A_val**(-1.0/self.ng_val) * eps_2**((1.0-self.ng_val)/(2.0*self.ng_val))
        nu = visc + self.gamma

        # Assemble form A
        a = nu*inner(sym(grad(self.u)),grad(self.v))*dx
        A = assemble(a, mat_type="aij", bcs = self.bcs)
        Ap = A.petscmat
        Ap = Ap.createSubMatrix(self.u_is, self.u_is)

        # Assemble form C
        c = sqrt(nu)*dot(self.u,self.v)*dx
        C = assemble(c, mat_type = "aij", bcs = self.bcs)
        Cp = C.petscmat
        Cp = Cp.createSubMatrix(self.u_is, self.u_is)
#
#        # 1.without lumping C
        diagC = PETSc.Mat.getDiagonal(Cp)
#
        # 2.lump matrix
#        ones = Cp.createVecLeft()
#        ones.set(1)
#        diagC = Cp.createVecLeft()
#        PETSc.Mat.mult(Cp,ones,diagC)

        CiACi = Ap.duplicate(copy = True)
        CiACi.diagonalScale(R = 1./diagC, L = 1./diagC)
        BCiACi = PETSc.Mat.matMult(self.Bp, CiACi)
        self.M_mat = PETSc.Mat.matTransposeMult(BCiACi,self.Bp)
#
#        # 3. Use diagonal of A
##        y,CiACi = [Cp.createVecLeft() for i in (0,1)]
##        diagA = PETSc.Mat.getDiagonal(Ap)
##        PETSc.Vec.pointwiseMult(y,diagA,1./diagC)
##        PETSc.Vec.pointwiseMult(CiACi,y,1./diagC)
##        BCiACi = self.Bp.duplicate(copy = True)
##        BCiACi.diagonalScale(R= CiACi, L = None)

        BCi = self.Bp.duplicate(copy = True)
        BCi.diagonalScale(R = 1./diagC, L = None)
        BCiBt = PETSc.Mat.matTransposeMult(BCi,self.Bp)

#        # Try BFBt without Newton method
#        BA = PETSc.Mat.matMult(self.Bp,Ap)
#        self.M_mat = PETSc.Mat.matTransposeMult(BA,self.Bp)
#        BCiBt = PETSc.Mat.matTransposeMult(self.Bp,self.Bp)

        # Construct middle block
        self.workspace = [self.M_mat.createVecLeft() for i in (0, 1)]

        # Construct lateral block
        Nksp = PETSc.KSP().create(comm=pc.comm)
        Nksp.setOperators(BCiBt)
        Nksp.setUp()
        Nksp.setFromOptions()
        self.Nksp = Nksp


    def apply(self, pc, x, y):
        a, b = self.workspace
        self.Nksp.solve(x, a)
        self.M_mat.mult(a, b)
        self.Nksp.solve(b, y)

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")





class DGWeightedMassInv(PCBase):

    def initialize(self, pc):
        self.update(pc)

    def update(self, pc):
#        print("update called"); sys.stdout.flush()
        _, P = pc.getOperators()
        appctx = self.get_appctx(pc)
        self.A_val = appctx["A_val"]
        self.eps_min_val = appctx["eps_min_val"]
        self.ng_val = appctx["ng_val"]
        self.gamma = appctx["gamma"]
        V = dmhooks.get_function_space(pc.getDM())
        # get function spaces
        u = TrialFunction(V)
        v = TestFunction(V)
        state = appctx["state"]
        u0 = split(state)[0]
        eps_2 = 0.5 * tr(dot(sym(grad(u0)), sym(grad(u0)))) + self.eps_min_val**2.0
        visc = 0.5 * self.A_val**(-1.0/self.ng_val) * eps_2**((1.0-self.ng_val)/(2.0*self.ng_val)) 
        nu = - visc - self.gamma

        massinv = assemble(Tensor(1./nu*inner(u, v)*dx).inv)
        self.massinv = massinv.petscmat

#        print("value of visc :: %.2f" % assemble(visc*dx) )
#        import matplotlib.pylab as plt
#        V_plt = FunctionSpace(V.mesh(), "DG", 0)
#        visc_plt = Function(V_plt)
#        visc_plt.interpolate(visc)
#        print("max visc = " + str(max(visc_plt.vector()[:])))
#        print("min visc = " + str(min(visc_plt.vector()[:])))
#        fig = plt.figure()
#        plot(visc_plt)
#        plt.savefig("output/visc.pdf")
#        plt.close(fig)

    def apply(self, pc, x, y):
        self.massinv.mult(x, y)

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")


class DGMassInv(PCBase):

    def initialize(self, pc):
        _, P = pc.getOperators()
        appctx = self.get_appctx(pc)
        V = dmhooks.get_function_space(pc.getDM())
        # get function spaces
        u = TrialFunction(V)
        v = TestFunction(V)
        massinv = assemble(Tensor(inner(u, v)*dx).inv)
        self.massinv = massinv.petscmat
        self.gamma = appctx["gamma"]

    def update(self, pc):
        pass

    def apply(self, pc, x, y):
        self.massinv.mult(x, y)
        scaling = float(1.0) + float(self.gamma)
        y.scale(-scaling)# Remove some parameters no longer necessary

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")
