from firedrake import *
from firedrake.petsc import PETSc
from alig import *
import numpy as np


class SquareProblem(NavierStokesProblem):
    def __init__(self, baseN, theta,
                 A_val, ng_val, eps_min_val):
        super().__init__()
        self.baseN = baseN
        self.theta = theta
        self.A_val = A_val
        self.ng_val = ng_val
        self.eps_min_val = eps_min_val

    def mesh(self, distribution_parameters):
        base = UnitSquareMesh(self.baseN, self.baseN,
                             distribution_parameters=distribution_parameters)
        return base

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0), Constant((0,0)), (1,2,3,4))]
        return bcs

    def has_nullspace(self): return False

    def exact(self,Z):
        x = SpatialCoordinate(Z.mesh())
        V_int = VectorFunctionSpace(Z.mesh() , "CG", 4)
        Q_int = FunctionSpace(Z.mesh() , "CG", 3)
        u_exact = interpolate(as_vector([(x[0]*(1-x[0]))**(self.theta+1)*(x[1]*(1-x[1]))**self.theta*(1-2*x[1]),
                                         -(x[0]*(1-x[0]))**self.theta*(x[1]*(1-x[1]))**(self.theta+1)*(1-2*x[0])]), V_int)
        p_exact = interpolate(x[0]*x[1]-0.25, Q_int)
        return [u_exact, p_exact]

    def rhs(self, Z):
        (u_exact,p_exact) = self.exact(Z)
        f = - div(2*physics.eta(u_exact, self.eps_min_val, self.A_val, self.ng_val)*physics.epsilon(u_exact)) + grad(p_exact)
        return [f,Constant(0.0)]

    def relaxation_direction(self): return "0+:1-"


if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--theta", type=float, default = 1.5)
    parser.add_argument("--n", type=float, default = 3.0)
    parser.add_argument("--eps", type=float, default = 1e-2)
    parser.add_argument("--A", type=float, default = 1e-9)
    args, _ = parser.parse_known_args()

    # Glen's law exponent
    ng_val = Constant(args.n)

    # regularsation to stop stress blowing up as strain goes to zero
    eps_min_val =  Constant(args.eps)

    # Viscosity parameter
    A_val = Constant(args.A)

    # Set problem and solver
    problem = SquareProblem(args.baseN, args.theta, A_val, ng_val, eps_min_val)
    solver = get_solver(args, problem)

    # Set monitor
    error = []
    def mymonitor(ksp, it, rnorm):
        error.append(rnorm)
    solver.solver.snes.ksp.setMonitor(mymonitor)

    # solve
    (z, results) = solver.solve(A_val, ng_val, eps_min_val)

    # Plot error
    from matplotlib import pylab
    pylab.plot(error)
    pylab.yscale('log')
    pylab.grid(True)
    pylab.savefig("error.png")

    (u_exact,p_exact) = problem.exact(z.function_space())
    u =  z.split()[0]
    p = z.split()[1]
    # (u, p) = z.split()
    grad_error = assemble(inner(grad(u-u_exact),grad(u-u_exact))*dx)
    error_u = np.sqrt(errornorm(u,u_exact)**2 + grad_error)
    error_p = errornorm(p,p_exact)
    print('error u :: ' + str(error_u))
    print('error p :: ' + str(error_p))
